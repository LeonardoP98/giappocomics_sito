$( document ).ready(function() {
    console.log( id_manga );
    popola_pagina(id_manga);



    function popola_pagina(id){

        $.ajax({
            url: "https://www.mangaeden.com/api/manga/"+id+"/", 
            type: "GET",
            
            success: function(result){
                console.log(result);
                $('#copertina').attr('src','https://cdn.mangaeden.com/mangasimg/'+result['image']);
                $('#titolo').text(result['alias']);
                $('#trama').text(result['description']);
                $('#autore').text((result['author']));
                $( "#capitoli" ).fadeOut( "fast", function() {
                    // Animation complete
                });
                
                capitoli=result['chapters'];
                capitoli.sort(function(a,b){
                    return a['0'] - b['0'];
                });
                testo="<ul>";
                for(capitolo of capitoli){
                    testo+= '<li onclick="leggiManga(\''+capitolo['3']+'\')">'+capitolo['0']+': '+capitolo['2']+'</li>';
                }
                testo+='</ul>';
                $('#capitoli').append(testo);
            }
        });

        
        
    }
    // With the element initially hidden, we can show it slowly:
    $( "#btnMostra" ).click(function() {
        $( "#capitoli" ).fadeIn( "slow", function() {
        // Animation complete
        });
    });

});


function leggiManga(id_capitolo){
    window.location.href = base_url+"lettura_manga/lettura_manga.php?cap_id="+id_capitolo;
}