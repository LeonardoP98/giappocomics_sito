<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Giappocomics</title>
    <link rel="stylesheet" href="Sito_anime.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">



    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
    <header id="intestazione">
        <div id="sopra"></div>
        <div id="logo"></div>
        <div id="menu">
            <a href="lista_manga/lista.html" class="menu">ANIME</a>
            <a href="lista_manga/lista.html" class="menu">MANGA</a>
            <a href="www.google.it">GALLERIA</a>
            <a href="www.google.it"> USCITE</a>
        </div>
    </header>

    <div id="container">
        <div id="myCarousel" class="carousel slide top_anime" data-ride="carousel"> 
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
                <li data-target="#myCarousel" data-slide-to="5"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="immagini/night_clan.jpg" alt="Los Angeles">
                </div>

                <div class="item">
                    <img src="immagini/anime_cha.jpg" alt="Chicago">
                </div>

                <div class="item">
                    <img src="immagini/ben venuto.jpg" alt="New York">
                </div>
                <div class="item">
                    <img src="immagini/kantai.jpg" alt="kantai">
                </div>

                <div class="item">
                    <img src="immagini/test400px.jpg" alt="Chicago">
                </div>

                <div class="item">
                    <img src="immagini/test450.jpg" alt="New York">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

            </div>
        </div>
    </div>
</div>





    <div id="container">


        <!-- <img src="immagini/img_logo.jpeg" alt="foto anime bello" class="poster"> -->


    </div>
    <footer id="piede">
        <p id="primo">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
            deserunt mollit anim id est laborum</p>
    </footer>
    <!-- <button class="btn btn-primary">ciao</button>
    <a class="btn btn-primary">ciao</a> -->




    <script src="Sito_anime.js"></script>
</body>

</html>